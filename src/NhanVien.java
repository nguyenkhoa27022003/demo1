public class NhanVien {
    private String name , diaChi , maNV ,gioiTinh;
    private String age;
    private String honNhan;
    public void NhanVien (){

    }
    public NhanVien(String name , String diaChi , String maNV , String gioiTinh, String age , String honNhan){
        this.name = name ;
        this.diaChi= diaChi ;
        this.maNV = maNV;
        this.gioiTinh = gioiTinh;
        this.age = age;
        this.honNhan = honNhan;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name= name;
    }
    public  String getDiaChi(){
        return diaChi;
    }
    public void setDiaChi(String diaChi){
        this.diaChi = diaChi;
    }
    public String getMaNV(){
        return maNV;
    }
    public void setMaNV(String maNV){
        this.maNV= maNV;
    }
    public String getGioiTinh(){
        return gioiTinh;
    }
    public void setGioiTinh(String gioiTinh){
        this.gioiTinh= gioiTinh;
    }
    public String getAge(){
        return age;
    }
    public void setAge(String age){
        this.age = age;
    }
    public  String getHonNhan (){
        return honNhan;
    }
    public void setHonNhan(String honNhan){
        this.honNhan=honNhan;
    }

}
