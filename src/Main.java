import java.util.ArrayList;
import java.util.*;

public class Main {
    public static List<NhanVien> danhSachNhanVien = new ArrayList<>();

    public static void main(String[] args) {

        //Danh sachs khach hang
        NhanVien a = new NhanVien("Khoa", "Ha Noi  ", "NV_1_Khoa", "nu", "23", "0");
        danhSachNhanVien.add(a);
        NhanVien b = new NhanVien("Nam", "Hai Phong", "NV_2_Nam", "nam", "23", "0");
        danhSachNhanVien.add(b);
        NhanVien c = new NhanVien("Đạt", "Nha Trang", "NV_3_Dat", "nam", "32", "1");
        danhSachNhanVien.add(c);
        NhanVien d = new NhanVien("Quốc", "Da Nang ", "NV_4_Quoc", "nam", "43", "1");
        danhSachNhanVien.add(d);
        NhanVien e = new NhanVien("Huy", "Sai Gon  ", "NV_5_Huy", "nam", "25", "0");
        danhSachNhanVien.add(e);

        //menu
        int soMenu;
        do {
            soMenu = inMenuQuanLyNhanVien();
            switch (soMenu) {
                case 1: {
                    //xem danh sách nhan vien
                    hienThiDanhSachNhanVien();
                    break;
                }
                case 2: {
                    // Thêm nhân viên
                    taoMoiNhanVien();
                    break;

                }
                case 3: {
                    //sửa nhân viên
                    hienThiDanhSachNhanVien();
                    suaNhanVien();
                    break;
                }
                case 4: {
                    //Xóa nhân viên
                    hienThiDanhSachNhanVien();
                    xoaNhanVien();

                    break;
                }
            }
        } while (soMenu != 0);
    }

    public static int inMenuQuanLyNhanVien() {
        System.out.println("==== Mời bạn chọn chương trình ====");
        System.out.println("* 1> Xem danh sách nhân viên");
        System.out.println("* 2> Thêm nhân viên ");
        System.out.println("* 3> Sửa nhân viên ");
        System.out.println("* 4> Xóa nhân viên");
        System.out.println("* 0> Thoát");
        System.out.print("Mời nhập số tương ứng với chương trình: ");
        Scanner scan = new Scanner(System.in);
        int so = scan.nextInt();
        System.out.print("Bạn đã chọn chương trình số : " + so + "\n");
        return so;
    }


    // hàm in ds nv
    public static int hienThiDanhSachNhanVien() {
        System.out.println("Danh sách nhân viên hiện tại là: ");
        System.out.print("Tên        Địa chỉ         Mã NV          Giới tính       Tuổi        Hôn nhân \n");
        for (int i = 0; i < danhSachNhanVien.size(); i++) {
            System.out.println(danhSachNhanVien.get(i).getName() + "       " + danhSachNhanVien.get(i).getDiaChi() + "        " + danhSachNhanVien.get(i).getMaNV() + "       " + danhSachNhanVien.get(i).getGioiTinh() + "       " + danhSachNhanVien.get(i).getAge() + "        " + danhSachNhanVien.get(i).getHonNhan());
        }
        return 0;
    }

    // hàm tạo thêm nhân viên
    public static void taoMoiNhanVien() {
        int soNhanVienThem;
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhập số nhân viên cần thêm: ");
        soNhanVienThem = scan.nextInt();
        System.out.println("Số nhân viên cần thêm là: " + soNhanVienThem);
        List<NhanVien> lstNhanVien = new ArrayList<>();
        for (int i = 1; i <= soNhanVienThem; i++) {
            int stt = danhSachNhanVien.size() + i;
            System.out.println("Nhập tên nhân viên thứ " + i + ": ");
            String name = scan.next();
            System.out.println("Nhập Địa chỉ: ");
            String diaChi = scan.next();
            String maNV =genId(name,stt);
            System.out.println("Mã nhân viên: " + maNV);
            System.out.println("Nhập giới tính: ");
            String gioiTinh = scan.next();
            System.out.println("Nhập tuổi: ");
            String age = scan.next();
            System.out.println("Tình trạng hôn nhân: ");
            String honNhan = scan.next();

            NhanVien nv = new NhanVien(name, diaChi, maNV, gioiTinh, age, honNhan);
            lstNhanVien.add(nv);

        }
        danhSachNhanVien.addAll(lstNhanVien);
        hienThiDanhSachNhanVien();
    }
    //ham sửa nhân viên
    public static void suaNhanVien() {
        //hienThiDanhSachNhanVien();
        System.out.println("Sửa thông tin nhân viên ");
        System.out.println("Nhập id nhân viên bạn muốn sửa: ");
        Scanner scanner = new Scanner(System.in);
        String idSua = scanner.next();

        boolean khongTimThay = false;
        for (int i = 0; i < danhSachNhanVien.size(); i++) {
            if (danhSachNhanVien.get(i).getMaNV().equals(idSua)) {
                System.out.println("Cập nhật tên mới: ");
                String name = scanner.next();
                System.out.println("Cập nhật địa chỉ mới: ");
                String diaChi = scanner.next();
                System.out.println("Cập nhật giới tính mới: ");
                String gioiTinh = scanner.next();
                System.out.println("Cập nhật tuổi mới: ");
                String age = scanner.next();
                System.out.println("Cập nhật tình trạng hôn nhân mới: ");
                String honNhan = scanner.next();

                NhanVien updated = danhSachNhanVien.get(i);
                updated.setName(name);
                updated.setDiaChi(diaChi);
                updated.setGioiTinh(gioiTinh);
                updated.setAge(age);
                updated.setHonNhan(honNhan);
                danhSachNhanVien.set(i, updated);
                khongTimThay = false;
                break;
            } else {
                khongTimThay = true;
            }
        }
        if (khongTimThay) {
            System.out.println("Khong tim thay nhan vien can sua!!!");
            khongTimThay = false;
        } else {
            // In lai danh sach sau sua
            hienThiDanhSachNhanVien();
        }
    }

    // hàm xóa nhân viên
    public static void xoaNhanVien() {
        System.out.print("Mời bạn chọn id cần xóa: ");
        Scanner scan = new Scanner(System.in);
        String idXoa = scan.next();
        System.out.print("id mà bạn muốn xóa: " + idXoa + "\n");
        // xac nhan xoa
        Scanner scanner = new Scanner(System.in);
        System.out.print("Bạn muốn xóa: Y/N");
        String xacNhanXoa = scanner.next();
        while (!Arrays.asList("Y", "N").contains(xacNhanXoa.toUpperCase())) {
            System.out.print("Sai cu phap vui long nhap- Bạn muốn xóa: Y/N");
            xacNhanXoa = scanner.next();
        }
        if ("Y".equals(xacNhanXoa.toUpperCase())) {
            // thuc hien xoa
            boolean check = false;
            for (int i = 0; i < danhSachNhanVien.size(); i++) {
                if (danhSachNhanVien.get(i).getMaNV().equals(idXoa)) {
                    danhSachNhanVien.remove(i);
                } else {
                    check = true;
                }
            }
            if (check) {
                System.out.println("Mã nhân viên không tồn tại");
            }
            System.out.println("Danh sách nhân viên sau khi xóa là: ");
            System.out.print("Tên        Địa chỉ         Mã NV       Giới tính       Tuổi        Hôn nhân \n");
            for (int i = 0; i < danhSachNhanVien.size(); i++) {
                System.out.println(danhSachNhanVien.get(i).getName() + "       " + danhSachNhanVien.get(i).getDiaChi() + "        " + danhSachNhanVien.get(i).getMaNV() + "       " + danhSachNhanVien.get(i).getGioiTinh() + "            " + danhSachNhanVien.get(i).getAge() + "                " + danhSachNhanVien.get(i).getHonNhan());
            }
        }
    }

    public static String genId(String name,int stt){
        String id = "NV_"+ stt + "_"+ name;
        return id;
    }
}
